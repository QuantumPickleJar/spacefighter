
#include "CollisionManager.h"


void CollisionManager::AddCollisionType(const CollisionType type1, const CollisionType type2, OnCollision callback)
{
	Collision c;
	c.Type1 = (type1 < type2) ? type1 : type2;
	c.Type2 = (type1 > type2) ? type1 : type2;
	c.Callback = callback;

	m_collisions.push_back(c);
}
//Called when two GameObjects collide with one another
void CollisionManager::CheckCollision(GameObject *pGameObject1, GameObject *pGameObject2)
{
	//retrieve and locally store the CollisionType of both GameObjects
	CollisionType t1 = pGameObject1->GetCollisionType();
	CollisionType t2 = pGameObject2->GetCollisionType();

	//if they're both the same, or if one of them is set to not be collide-able, exit the method.
	if (t1 == t2 || t1 == CollisionType::NONE || t2 == CollisionType::NONE) return;

	//if one of them takes more priority 
	bool swapped = false;
	if (t1 > t2)
	{
		std::swap(t1, t2);
		swapped = true;
	}
	//declare iterator through all possible types of non-collision matches to decide how to handle the collision
	m_nonCollisionIt = m_nonCollisions.begin();
	for (; m_nonCollisionIt != m_nonCollisions.end(); m_nonCollisionIt++)
	{
		//make a pointer to the value at this index
		NonCollision nc = *m_nonCollisionIt;
		//if they're all the same, exit since nothing will happen
		if ((nc.Type1 == t1 && nc.Type2 == t2)) return;
	}
	
	//declare iterator through all possible types of collision matches to decide how to handle the collision
	m_collisionIt = m_collisions.begin();
	for (; m_collisionIt != m_collisions.end(); m_collisionIt++)
	{
		//make a pointer to the value at this index
		Collision c = *m_collisionIt;
		if ((c.Type1 == t1 && c.Type2 == t2))
		{
			//find the differnet between the two gameobjects' positions
			Vector2 difference = pGameObject1->GetPosition() - pGameObject2->GetPosition();

			//get the boundaries of each object
			float radiiSum = pGameObject1->GetCollisionRadius() + pGameObject2->GetCollisionRadius();
			float radiiSumSquared = radiiSum * radiiSum;
			
			//if the difference between the two is less than the square of both the radius, it means that they overlap and therefore have collided with each other.
			if (difference.LengthSquared() <= radiiSumSquared)
			{
				//make a call to the level since that's  where the collision manager is configurated 
				//if the second gameobject didn't take priority over the first one then reflect that in return type
				if (!swapped) c.Callback(pGameObject1, pGameObject2); 
				else c.Callback(pGameObject2, pGameObject1);
			}
			return;
		}
	}
	//if we didn;t find a match, we'll need one in the future for these two to keep the game running smoothly
	AddNonCollisionType(t1, t2);
}

void CollisionManager::AddNonCollisionType(const CollisionType type1, const CollisionType type2)
{
	NonCollision nc;
	nc.Type1 = (type1 < type2) ? type1 : type2;
	nc.Type2 = (type1 > type2) ? type1 : type2;
	m_nonCollisions.push_back(nc);
}