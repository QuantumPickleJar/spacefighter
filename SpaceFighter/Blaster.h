
#pragma once

#include "Weapon.h"


class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.1;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	//method called when player inputs fire control
	virtual void Fire(TriggerType triggerType)
	{
		//check if the weapon is able to be firedd
		if (IsActive() && CanFire())
		{
			//if it can be fired, check how it's supposed to be fired
			if (triggerType.Contains(GetTriggerType()))
			{
				//if it involves a projectile, shoot it.
				Projectile *pProjectile = GetProjectile();
				if (pProjectile)
				{
					//
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};